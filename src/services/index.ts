import { AppDataSource } from "../database/database";
import CryptoBlock from "../models/CryptoBlock";
import CryptoBlockchain from "../models/CryptoBlockchain";
import { Block } from "../models/entities/Block";

export const startGenesis = async () => {
  let smashingCoin = new CryptoBlockchain();
  let firstBlock: CryptoBlock = smashingCoin.obtainLatestBlock();
  // inserto el primer bloque
  let block = new Block();
  block.hash = firstBlock.getHash();
  block.date = new Date();
  block.indice = 0;
  return await AppDataSource.manager
    .save(block)
    .then((data) => {
      console.log(data);
      return data;
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

export const getAllBlocks = async () => {
  return await AppDataSource.manager
    .find(Block)
    .then((data) => {
      console.log(data);
      return data;
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

export const allBlocksDecrypted = async () => {
  return await AppDataSource.manager
    .find(Block)
    .then((data) => {
      let dataBlocks: any[] = [];
      data.forEach((block: Block, index: number) => {
        if (index === 0) return;
        let objectBlock = block.getDecryptedData();
        console.log(objectBlock);
        dataBlocks.push(objectBlock);
      });
      return dataBlocks;
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

export const publicarNota = async (payload: any) => {
  // Buscar el ultimo bloque
  return await AppDataSource.manager
    .find(Block, { order: { id: "desc" } })
    .then(async (latestBlock) => {
      console.log(latestBlock);
      if (latestBlock.length > 0) {
        // Crear el nuevo bloque
        let latest = latestBlock[0];
        let block = new CryptoBlock(
          Number(latest?.indice) + 1,
          new Date(),
          payload,
          latest?.hash
        );

        let notaBlock = new Block();
        notaBlock.indice = block.getIndex();
        notaBlock.date = block.getTimestamp();
        notaBlock.hash = block.getHash();
        notaBlock.hashPredecesor = block.getPrecedingHash();
        notaBlock.idBlockPredecesor = Number(latest?.id);

        return await AppDataSource.manager
          .save(notaBlock)
          .then((data) => {
            console.log(data);
            return data;
          })
          .catch((err) => {
            console.log(err);
            throw err;
          });
      }
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

export const getNotas = async (idRegistro: number) => {
  return await AppDataSource.manager
    .find(Block)
    .then((data) => {
      let dataBlocks: any[] = [];
      data.forEach((block: Block, index: number) => {
        if (index === 0) return;
        let objectBlock = block.getDecryptedData();
        console.log(objectBlock);
        dataBlocks.push(objectBlock);
      });

      let notasFiltradas = dataBlocks.filter((nota) => {
        return nota.idRegistro === idRegistro;
      });

      return notasFiltradas;
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};
