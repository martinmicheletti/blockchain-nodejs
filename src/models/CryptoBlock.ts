import { SHA512, AES, enc } from "crypto-js";

export default class CryptoBlock {
  private index: number;
  private timestamp: Date;
  private precedingHash: string;
  private hash: string;

  constructor(index: number, timestamp: Date, data: any, precedingHash = "") {
    this.index = index;
    this.timestamp = timestamp;
    this.precedingHash = precedingHash;
    this.hash = this.computeHash(data);
  }

  computeHash(data: any): string {
    let key = `${JSON.stringify(data)}`;
    let encrypted = AES.encrypt(
      key,
      `${this.index}${this.precedingHash}${this.timestamp}${process.env.PRIVATE_KEY}`
    ).toString();
    return encrypted;
  }
  // computeHash(data: any): string {
  //   let key = `${this.index}${this.precedingHash}${
  //     this.timestamp
  //   } data: ${JSON.stringify(data)}`;
  //   let encrypted = AES.encrypt(key, `${process.env.PRIVATE_KEY}`).toString();
  //   return encrypted;
  // }

  decryptData() {
    let decrypted = AES.decrypt(
      this.hash,
      `${this.index}${this.precedingHash}${this.timestamp}${process.env.PRIVATE_KEY}`
    ).toString(enc.Utf8);
    return decrypted;
  }

  getIndex() {
    return this.index;
  }

  getHash() {
    return this.hash;
  }
  getTimestamp() {
    return this.timestamp;
  }

  getPrecedingHash() {
    return this.precedingHash;
  }

  setHash(hash: string) {
    this.hash = hash;
  }

  setPrecedingHash(precedingHash: string) {
    this.precedingHash = precedingHash;
  }

  // getDecryptedData() {
  //   return JSON.parse(this.decryptData().split("data: ")[1]);
  // }
  getDecryptedData() {
    return JSON.parse(this.decryptData());
  }
}
