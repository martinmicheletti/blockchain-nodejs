import CryptoBlock from "./CryptoBlock";

export default class CryptoBlockchain {
  private blockchain: CryptoBlock[];

  constructor() {
    this.blockchain = [this.startGenesisBlock()];
  }

  startGenesisBlock() {
    return new CryptoBlock(0, new Date(), "Initial Block in the Chain", "0");
  }

  obtainLatestBlock() {
    return this.blockchain[this.blockchain.length - 1];
  }

  addNewBlock(newBlock: CryptoBlock) {
    newBlock.setPrecedingHash(this.obtainLatestBlock().getHash());
    newBlock.setHash(newBlock.computeHash(newBlock.getDecryptedData()));

    this.blockchain.push(newBlock);
  }

  getDecryptedBlocks() {
    let dataBlocks: any[] = [];
    debugger;
    this.blockchain.forEach((block) => {
      debugger;
      let objectBlock = block.getDecryptedData();
      console.log(objectBlock);
      dataBlocks.push(objectBlock);
      console.log(dataBlocks);
    });
    return dataBlocks;
  }

  checkChainValidity() {
    for (let i = 1; i < this.blockchain.length; i++) {
      const currentBlock = this.blockchain[i];
      const precedingBlock = this.blockchain[i - 1];

      if (
        currentBlock.getHash() !==
        currentBlock.computeHash(JSON.parse(currentBlock.getDecryptedData()))
      ) {
        return false;
      }
      if (currentBlock.getPrecedingHash() !== precedingBlock.getHash())
        return false;
    }
    return true;
  }
}
