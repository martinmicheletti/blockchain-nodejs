import { AES, enc } from "crypto-js";
import { Column, Entity, PrimaryGeneratedColumn, Timestamp } from "typeorm";

@Entity("Block")
export class Block {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id!: number;
  @Column("int", { name: "indice" })
  indice!: number;
  @Column("text", { name: "hash" })
  hash!: string;
  @Column("text", { name: "hashPredecesor" })
  hashPredecesor!: string;
  @Column("int", { name: "idBlockPredecesor" })
  idBlockPredecesor!: number;
  @Column({ type: "timestamp", name: "date" })
  date!: Date;
  constructor() {}
  decryptData() {
    let decrypted = AES.decrypt(
      this.hash,
      `${this.indice}${this.hashPredecesor}${this.date}${process.env.PRIVATE_KEY}`
    ).toString(enc.Utf8);
    return decrypted;
  }
  getDecryptedData() {
    return JSON.parse(this.decryptData());
  }
  // getDecryptedData() {
  //   return JSON.parse(this.decryptData().split("data: ")[1]);
  // }
}
