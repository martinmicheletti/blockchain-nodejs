import express, { json, Request, Response, urlencoded } from "express";
import cors from "cors";
import { config } from "dotenv";
import { AppDataSource } from "./database/database";

import router from "./routes";

config();
const app = express();
const port = process.env.PORT || 5000;
const corsOptions = {
  origin: "*",
  optionsSucessStatus: 200,
};

app.use(json({ limit: "30mb" }));
app.use(cors(corsOptions));

app.use("/", router);

const startServer = async () => {
  await app.listen(port, () => {
    console.log(`Server running on http://127.0.0.1:${port}`);
  });
};

(async () => {
  await AppDataSource.initialize()
    .then(async (data) => {
      await startServer();
    })
    .catch((error) => {
      console.log(error);
    });
})();
