import { Request, Response, Router } from "express";
import {
  allBlocksDecrypted,
  getAllBlocks,
  getNotas,
  publicarNota,
  startGenesis,
} from "../services";

const router = Router();

router.get("/", (req: Request, res: Response) => {
  res.status(200).json("Blockchain");
});

router.get("/start", async (req: Request, res: Response) => {
  await startGenesis()
    .then((data) => {
      console.log(data);
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500);
    });
});

router.get("/allBlocks", async (req: Request, res: Response) => {
  await getAllBlocks()
    .then((data) => {
      console.log(data);
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500);
    });
});

router.get("/allBlocksDecrypted", async (req: Request, res: Response) => {
  await allBlocksDecrypted()
    .then((data) => {
      console.log(data);
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500);
    });
});

router.post("/nota", async (req: Request, res: Response) => {
  await publicarNota(req.body)
    .then((data) => {
      console.log(data);
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500);
    });
});

router.get("/notas", async (req: Request, res: Response) => {
  let { idRegistro } = req.query;
  await getNotas(Number(idRegistro))
    .then((data) => {
      console.log(data);
      res.status(200).json(data);
    })
    .catch((err) => {
      res.status(500);
    });
});

export default router;
